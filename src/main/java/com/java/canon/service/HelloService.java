package com.java.canon.service;

import java.util.List;

import com.java.canon.entity.RegBean;

public interface HelloService {

	String getHelloWorldMsg();
	public List<RegBean> getAllSupplier(String formModel);
}
