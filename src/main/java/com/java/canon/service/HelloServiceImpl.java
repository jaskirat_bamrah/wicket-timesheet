package com.java.canon.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.canon.entity.RegBean;
import com.java.canon.view.Dao.HelloServiceDoa;


@Service
public class HelloServiceImpl  implements HelloService {

	@Autowired
	private HelloServiceDoa helloServiceDoa;
	
	public String getHelloWorldMsg() {
	return	helloServiceDoa.getHelloWorldMsg();
	}

	@Override
	public List<RegBean> getAllSupplier(String formModel) {
		return helloServiceDoa.getAllSupplier(formModel);
	}

}