package com.java.canon.view.Dao;

import java.util.List;

import com.java.canon.entity.RegBean;

public interface HelloServiceDoa {
	String getHelloWorldMsg();
	public List<RegBean> getAllSupplier(String formModel);
}
