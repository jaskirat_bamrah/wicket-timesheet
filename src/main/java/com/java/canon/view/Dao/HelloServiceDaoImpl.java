package com.java.canon.view.Dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.java.canon.entity.RegBean;
import com.java.canon.service.HelloServiceImpl;

@Repository
public class HelloServiceDaoImpl  implements HelloServiceDoa {
	 
		@Autowired
		private SessionFactory sessionFactory;

		
		 private static Logger LOG = Logger.getLogger(HelloServiceImpl.class);
		@Override
		public String getHelloWorldMsg() {
			return "Spring : hello world";
		}

		@SuppressWarnings("unchecked")
		@Override
		@Transactional
		public List<RegBean> getAllSupplier(String formModel) {
			Session session=null;
			try{
		 session = sessionFactory.getCurrentSession();
			}catch(Exception e){
				LOG.error("message is "+e);
			}
			 Query criteria = session.createQuery(formModel);
			LOG.error("criteria is "+criteria);
			LOG.error("message is "+criteria.list().size());
			return	(List<RegBean>) criteria.list(); 
			
		//	session.close();
		}

}
