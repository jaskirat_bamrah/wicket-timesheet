package com.java.canon.view;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigation;
import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigator;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.java.canon.entity.RegBean;
import com.java.canon.service.HelloService;

@SuppressWarnings("unused")
public class HomePage extends WebPage {

    @SpringBean
    public SpringInterface inteface;
    
    @SpringBean
    public HelloService helloService;

    private static final long serialVersionUID = 1L;

    private static Logger LOG = Logger.getLogger(HelloService.class);
    
	public HomePage() {
		PageableListView<?> pagableListView = new PageableListView<Object>("rows", getUserModelList(), 10) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 5569512213371647514L;

			@Override
			protected void populateItem(ListItem<Object> item) {
				RegBean userModel = (RegBean) item.getDefaultModelObject();
				item.add(new Label("name", userModel.getName()));
				item.add(new Label("gender", userModel.getEmailId()));
			} 
		};
		pagableListView.setOutputMarkupId(true);
		pagableListView.setOutputMarkupPlaceholderTag(true);
		add(pagableListView);
		AjaxPagingNavigator ajaxPagingNavigator=new AjaxPagingNavigator("nav", pagableListView);
		ajaxPagingNavigator.setOutputMarkupId(true);
		ajaxPagingNavigator.setOutputMarkupPlaceholderTag(true);
		add(ajaxPagingNavigator);
	}

	private List<RegBean> getUserModelList() {
		LOG.info("method getUserModelList invoked at controller level ");
		List<RegBean> test = helloService.getAllSupplier("from RegBean");
		LOG.info("method getUserModelList invoked at controller level "+test.size());
		return test;
	}
}
