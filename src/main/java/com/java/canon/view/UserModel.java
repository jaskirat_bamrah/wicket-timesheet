package com.java.canon.view;

import java.io.Serializable;

public class UserModel implements Serializable{

	
	private static final long serialVersionUID = -1906049405090900466L;
	private String name;
	private String gender;
	
	
	
	public UserModel(String name, String gender) {
		super();
		this.name = name;
		this.gender = gender;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	
}
