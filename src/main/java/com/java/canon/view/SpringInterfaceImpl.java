package com.java.canon.view;

import org.springframework.stereotype.Component;



@Component
public class SpringInterfaceImpl implements SpringInterface{

    @Override
    public String getString() {
        return "Hello";
    }

}
