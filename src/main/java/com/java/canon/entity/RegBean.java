package com.java.canon.entity;

import java.io.Serializable;
import java.util.Date;
 
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@javax.persistence.Entity 
@Table(name="users")
public class RegBean implements Serializable {
 
private static final long serialVersionUID = 693797831486419717L;

@Id
@GeneratedValue
@Column(name="regId")
private String regId="";
private String name;
private String emailId;
 
@Temporal(TemporalType.DATE)
@Column (name="createdOn")
private Date createdOn;
 
public Date getCreatedOn() {
return new Date();
//return createdOn;
}
public void setCreatedOn(Date createdOn) {
this.createdOn = createdOn;
}
public String getRegId() {
return regId;
}
public void setRegId(String regId) {
this.regId = regId;
}
@Column(name="name")
public String getName() {
return name;
}
public void setName(String name) {
this.name = name;
}
@Column(name="emailId")
public String getEmailId() {
return emailId;
}
public void setEmailId(String emailId) {
this.emailId = emailId;
}
 
}